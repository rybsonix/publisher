<?php

// SINGLE PRODUCT PAGE
// HIDE CATEGORY
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

// CHANGE LOCATION OF STOCK INFO
function stock_replace_on_single_product() {
	global $product;
	echo wc_get_stock_html( $product ); // WPCS: XSS ok.
}
add_action( 'woocommerce_single_product_summary', 'stock_replace_on_single_product', 15 );

function publisher_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'publisher_add_woocommerce_support' );


function single_product_additional_fragment (){
	if( get_field('fragment-link')){
		echo '<p class="additional-fragment"><a class="button-underline" target="_blank" href="'.get_field('fragment-link').'">fragment (PDF)</a></p>';
	}
};
add_action('woocommerce_after_add_to_cart_button', 'single_product_additional_fragment');


function remove_product_thumb_gallery_under_main_thumb() {
	remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );
}
add_action( 'woocommerce_product_thumbnails', 'remove_product_thumb_gallery_under_main_thumb', 5 );


function publisher_wc_output_long_description() {
	?>
	<div class="woocommerce-tabs">
		<div class="single-product-page-content-container">
			<div class="single-product-page__long-desc">
				<?php the_content(); ?>
			</div>
			<div class="grid-x grid-margin-x">
				<div class="cell small-12 large-5">
					<div class="single-product-page__attr-container">
						<?php global $product;
						$page_count = $product->get_attribute( 'pa_liczba-stron' );
						$translate = $product->get_attribute( 'pa_tlumaczenie' );
						$isbn = $product->get_attribute( 'pa_isbn' );
						$publish_year = $product->get_attribute( 'pa_rok-wydania' );
						$publish_format = $product->get_attribute( 'pa_wydanie' );
						?>
						<div class="grid-x">
							<?php
							if ( $page_count ) {
								echo '<div class="cell small-12 medium-5"><p>Liczba stron:</p></div><div class="cell small-12 medium-7"><span>'.$page_count.'</span></div>';
							};
							if ( $translate ) {
								echo '<div class="cell small-12 medium-5"><p>Tłumaczenie:</p></div><div class="cell small-12 medium-7"><span>'.$translate.'</span></div>';
							};
							if ( $isbn ) {
								echo '<div class="cell small-12 medium-5"><p>ISBN:</p></div><div class="cell small-12 medium-7"><span>'.$isbn.'</span></div>';
							};
							if ( $publish_year ) {
								echo '<div class="cell small-12 medium-5"><p>Rok wydania:</p></div><div class="cell small-12 medium-7"><span>'.$publish_year.'</span></div>';
							};
							if ( $publish_format ) {
								echo '<div class="cell small-12 medium-5"><p>Wydanie:</p></div><div class="cell small-12 medium-7"><span>'.$publish_format.'</span></div>';
							};
							?>
						</div>
					</div>
				</div>
				<div class="cell small-12 large-7">
					<div class="single-product-page__delivery-container">
						<?php if( have_rows('woo_delivery_options','option') ): ?>
							<?php while( have_rows('woo_delivery_options','option') ): the_row();?>
								<div class="single-product-page__delivery-box">
									<?php
									$image = get_sub_field('woo_delivery_option_img','option');
									$text = get_sub_field('woo_delivery_option_text','option');
									?>
									<div class="single-product-page__delivery-box-img">
										<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
									</div>
									<p><?php echo $text; ?></p>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_after_single_product_summary', 'publisher_wc_output_long_description', 10 );


remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'up_sells_loop_product_authot_attr_after_title', 10 );
function up_sells_loop_product_authot_attr_after_title() {
	$title = get_the_title();
	global $product;
	$taxonomy = 'pa_autor';
	$link = get_permalink();
	$value = $product->get_attribute( $taxonomy );
	echo '<h2 class="woocommerce-loop-product__title"><a href="' . $link . '">' . $title . '</a></h2>';
	if ( $value ) {
		echo '<p>' . $value . '</p>';
	}
}


function publisher_change_number_related_products( $args ) {
	$args['posts_per_page'] = 3;
	$args['columns'] = 3;
	return $args;
}
add_filter( 'woocommerce_upsell_display_args', 'publisher_change_number_related_products', 9999 );


function invert_formatted_sale_price( $price, $regular_price, $sale_price ) {
	return '<ins>' . ( is_numeric( $sale_price ) ? wc_price( $sale_price ) : $sale_price ) . '</ins> <del>' . ( is_numeric( $regular_price ) ? wc_price( $regular_price ) : $regular_price ) . '</del>';
}
add_filter( 'woocommerce_format_sale_price', 'invert_formatted_sale_price', 10, 3 );


function woocommerce_custom_single_product_page_add_to_cart_text() {
	return __( 'do koszyka', 'woocommerce' );
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_product_page_add_to_cart_text' );

function publisher_loop_product_thumbnail() {
	global $product;
	$link = get_permalink();
	$size = 'woocommerce_single';
	$image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

	echo '<a href="' . $link . '">' . $product->get_image( $image_size ) . '</a>';
}
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'publisher_loop_product_thumbnail', 10 );

// SHOP PAGE
//Hide categories from Woocommerce product category widget
function pub_woo_product_cat_widget_args( $cat_args ) {
	$cat_args['exclude'] = array('17','19','23');
	return $cat_args;
}
add_filter( 'woocommerce_product_categories_widget_args', 'pub_woo_product_cat_widget_args' );

// CART
function woocommerce_button_proceed_to_checkout() { ?>
	<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button-underline">
		<?php _e( 'zamawiam <br /> z obowiązkiem <br /> zapłaty', 'woocommerce' ); ?>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow-big.svg" alt="" role="button">
	</a>
	<?php
}

function display_zero_to_shipping_label_for_zero_cost( $label, $method ) {
	// If shipping method cost is 0 or null, display 'Free' (except for free shipping method)
	if ( ! ( $method->cost > 0 ) && $method->method_id !== 'free_shipping' ) {
		$label .= ': ' . __('<span class="woocommerce-Price-amount amount"><bdi>0,00&nbsp;<span class="woocommerce-Price-currencySymbol">zł</span></bdi></span>');
	}
	return $label;
}
add_filter( 'woocommerce_cart_shipping_method_full_label', 'display_zero_to_shipping_label_for_zero_cost', 10, 2 );

// REMOVE COUPON BUTTON
function pub_woocommerce_cart_totals_coupon_html( $coupon_html, $coupon, $discount_amount_html ) {
    $coupon_html = $discount_amount_html . ' <a href="' . esc_url( add_query_arg( 'remove_coupon', urlencode( $coupon->get_code() ), defined( 'WOOCOMMERCE_CHECKOUT' ) ? wc_get_checkout_url() : wc_get_cart_url() ) ) . '" class="woocommerce-remove-coupon" data-coupon="' . esc_attr( $coupon->get_code() ) . '">' . __( 'X', 'woocommerce' ) . '</a>';

    return $coupon_html;
}
add_filter( 'woocommerce_cart_totals_coupon_html', 'pub_woocommerce_cart_totals_coupon_html', 10, 3 );

// CHECKOUT
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'woocommerce_after_checkout_form', 'woocommerce_checkout_coupon_form' );

// CREATE ACCOUNT INPUT CHECKED ON CHECKOUT
// add_filter('woocommerce_create_account_default_checked' , function ($checked){
//     return true;
// });

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'pub_override_checkout_fields' );

// Our hooked in function – $fields is passed via the filter!
function pub_override_checkout_fields( $fields ) {
     $fields['billing']['billing_nip'] = array(
        'label'     => __('NIP', 'woocommerce'),
    	'placeholder'   => _x('NIP (opcjonalnie)', 'placeholder', 'woocommerce'),
    	'required'  => false,
    	'class'     => array('form-row-wide'),
    	'clear'     => true,
		'priority' => 35,
     );
     return $fields;
}
/**
 * Display field value on the admin order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'pub_checkout_field_display_admin_order_meta', 10, 1 );
function pub_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('NIP').':</strong> ' . get_post_meta( $order->get_id(), '_billing_nip', true ) . '</p>';
}

// Billing fields on my account edit-addresses and checkout
add_filter( 'woocommerce_billing_fields' , 'pub_billing_fields' );
function pub_billing_fields( $fields ) {
    // Billing Fields
    $fields['billing_nip']['label'] = 'NIP';
	$fields['billing_nip']['priority'] = 35;
    return $fields;
}

// ORDER BUTTON HTML
function pub_custom_button_html( $button_html ) {
	$order_button_text = 'Potwierdzam zamówienie';
	$button_html = '<button type="submit" class="button button-underline" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>';
	return $button_html;
}
add_filter( 'woocommerce_order_button_html', 'pub_custom_button_html' );

// REORDER REVIEW ORDER FRAGMENTS
add_action( 'woocommerce_checkout_order_review', 'reordering_checkout_order_review', 1 );
function reordering_checkout_order_review(){
	remove_action('woocommerce_checkout_order_review','woocommerce_checkout_payment', 20 );
	add_action( 'woocommerce_checkout_order_review', 'custom_checkout_payment', 8 );
	add_action( 'woocommerce_checkout_order_review', 'custom_checkout_place_order', 20 );
}

function custom_checkout_payment() {
	$checkout = WC()->checkout();
	if ( WC()->cart->needs_payment() ) {
		$available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
		WC()->payment_gateways()->set_current_gateway( $available_gateways );
	} else {
		$available_gateways = array();
	}

	if ( ! is_ajax() ) {
		// do_action( 'woocommerce_review_order_before_payment' );
	}
	?>
	<div id="payment" class="woocommerce-checkout-payment-gateways">
		<?php if ( WC()->cart->needs_payment() ) : ?>
			<ul class="wc_payment_methods payment_methods methods">
				<?php
				if ( ! empty( $available_gateways ) ) {
					foreach ( $available_gateways as $gateway ) {
						wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
					}
				} else {
					echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">';
					echo apply_filters( 'woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) : esc_html__( 'Please fill in your details above to see available payment methods.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
				}
				?>
			</ul>
		<?php endif; ?>
	</div>
	<?php
}

function custom_checkout_place_order() {
	$checkout          = WC()->checkout();
	$order_button_text = apply_filters( 'woocommerce_order_button_text', __( 'Place order', 'woocommerce' ) );
	?>
	<div id="payment-place-order" class="woocommerce-checkout-place-order">
		<div class="form-row place-order">
			<noscript>
				<?php esc_html_e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
				<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
			</noscript>

			<?php wc_get_template( 'checkout/terms.php' ); ?>

			<?php do_action( 'woocommerce_review_order_before_submit' ); ?>

			<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

			<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

			<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
		</div>
	</div>
	<?php
	if ( ! is_ajax() ) {
		do_action( 'woocommerce_review_order_after_payment' );
	}
}

// CHECKOUT INPUT QUANTITY AJAX LOAD
function webroom_add_quanity_js(){
	if ( is_checkout() ) {
		$localize_script = array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		);
		wp_localize_script( 'site-js', 'add_quantity', $localize_script );
	}
}
add_action( 'wp_footer', 'webroom_add_quanity_js', 10 );

function webroom_load_ajax() {
	if ( !is_user_logged_in() ){
		add_action( 'wp_ajax_nopriv_update_order_review', 'webroom_update_order_review' );
	} else{
		add_action( 'wp_ajax_update_order_review', 'webroom_update_order_review' );
	}
}
add_action( 'init', 'webroom_load_ajax' );
function webroom_update_order_review() {
	$values = array();
	parse_str($_POST['post_data'], $values);
	$cart = $values['cart'];
	foreach ( $cart as $cart_key => $cart_value ){
		WC()->cart->set_quantity( $cart_key, $cart_value['qty'], false );
		WC()->cart->calculate_totals();
		woocommerce_cart_totals();
	}
	wp_die();
}

// CHECKOUT LOGIN FORM
// remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
// add_action( 'woocommerce_after_checkout_billing_form', 'woocommerce_checkout_login_form' );

// CREATE ACCOUNT FORM WITHOUT CHECKBOX
// add_filter('woocommerce_create_account_default_checked' , function ($checked){
//     return true;
// });

// HIDE SHIP TO DIFFERENT ADDRESS CHECKBOX IF SHIPPING METHOD IS SELF COLLECT
function pub_disable_shipping_local_pickup( $available_gateways ) {

   // Part 1: Hide shipping based on the static choice @ Cart
   $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
   $chosen_shipping = $chosen_methods[0];
   if ( 0 === strpos( $chosen_shipping, 'local_pickup' ) ) {
   ?>
      <script type="text/javascript">
         jQuery('#ship-to-different-address').fadeOut();
      </script>
   <?php
   }
   // Part 2: Hide shipping based on the dynamic choice @ Checkout
   ?>
      <script type="text/javascript">
         jQuery('form.checkout').on('change','input[name^="shipping_method"]',function() {
            var val = jQuery( this ).val();
            if (val.match("^local_pickup")) {
                     jQuery('#ship-to-different-address').fadeOut();
               } else {
               jQuery('#ship-to-different-address').fadeIn();
            }
         });
      </script>
   <?php
}
add_action( 'woocommerce_after_checkout_form', 'pub_disable_shipping_local_pickup' );

// add_action( 'wp_footer', 'woocommerce_show_coupon', 99 );
// function woocommerce_show_coupon() {
// 	echo '
// 	<script type="text/javascript">
// 	jQuery(document).ready(function(event) {
// 		jQuery(\'.checkout_coupon\').show();
// 	});
// 	</script>
// 	';
// }

// MY ACCOUNT
// REGISTRATION CONFIRM PASSWORD
function woocommerce_registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
	global $woocommerce;
	extract( $_POST );
	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_filter('woocommerce_registration_errors', 'woocommerce_registration_errors_validation', 10, 3);

function woocommerce_register_form_password_repeat() {
	?>
	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide form-row-padding-x">
		<span class="password-input">
			<!-- <label for="reg_password2"><?php //_e( 'Confirm password', 'woocommerce' ); ?> <span class="required">*</span></label> -->
			<input type="password" class="input-text" name="password2" placeholder="powtórz hasło" autocomplete="new-password" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
		</span>
	</p>
	<?php
}
add_action( 'woocommerce_register_form', 'woocommerce_register_form_password_repeat' );

// REGISTRATION FORM CONSENT
function pub_add_registration_privacy_policy() {
	woocommerce_form_field( 'privacy_policy_reg', array(
	   'type'          => 'checkbox',
	   'class'         => array('form-row privacy'),
	   'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
	   'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
	   'required'      => true,
	   'label'         => '<span class="checkmark"></span>Zakładając konto w naszym serwisie zgadzasz się z <a href="/polityka-prywatnosci/">polityką prywatności</a>',
	));
}
add_action( 'woocommerce_register_form', 'pub_add_registration_privacy_policy', 11 );

// Show error if user does not tick
function pub_validate_privacy_registration( $errors, $username, $email ) {
	if ( ! is_checkout() ) {
	    if ( ! (int) isset( $_POST['privacy_policy_reg'] ) ) {
	        $errors->add( 'privacy_policy_reg_error', __( 'Wymagana jest zgoda przy Polityce prywatności!', 'woocommerce' ) );
	    }
	}
	return $errors;
}
add_filter( 'woocommerce_registration_errors', 'pub_validate_privacy_registration', 10, 3 );

// MY ACCOUNT
// HIDE NAVIGATION TABS
function pub_remove_downloads_dashboard_edit_address_my_account( $items ) {
	unset($items['dashboard']);
   	unset( $items['downloads']);
	unset($items['edit-address']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'pub_remove_downloads_dashboard_edit_address_my_account', 9999 );

// MERGE NAVIGATION TABS
// 2. Second, print the ex tab content into an existing tab (edit-account in this case)
add_action( 'woocommerce_account_edit-account_endpoint', 'woocommerce_account_edit_address' );

//REDIRECTS
function pub_redirect_to_orders_from_dashboard(){
	if( is_user_logged_in() && is_account_page() && empty( WC()->query->get_current_endpoint() ) ){
		wp_safe_redirect( wc_get_account_endpoint_url( 'orders' ) );
		exit;
	}
}
add_action('template_redirect', 'pub_redirect_to_orders_from_dashboard' );

function pub_woocommerce_customer_save_address( $user_id, $load_address ) {
       wp_safe_redirect(wc_get_account_endpoint_url('edit-account'));
       exit;
};
add_action( 'woocommerce_customer_save_address', 'pub_woocommerce_customer_save_address', 99, 2 );

function pub_woocommerce_customer_save_account_details( $user_id ) {
       wp_safe_redirect(wc_get_account_endpoint_url('edit-account'));
       exit;
};
add_action( 'woocommerce_save_account_details', 'pub_woocommerce_customer_save_account_details', 12, 1 );

// ORDER DETAILS TABLE COLUMN ORDER
function pub_orders_columns( $columns = array() ) {
    // Hide the columns
    if( isset($columns['order-total']) ) {
        // Unsets the columns which you want to hide
        unset( $columns['order-date'] );
		unset( $columns['order-status'] );
		unset( $columns['order-total'] );
		unset( $columns['order-actions'] );
    }
	$columns['order-status'] = __( 'Status', 'Text Domain' );
	$columns['order-title'] = __( 'Tytuł', 'Text Domain' );
	$columns['order-actions'] = __( '&nbsp;', 'Text Domain' );
    return $columns;
}
add_filter( 'woocommerce_account_orders_columns', 'pub_orders_columns' );

// VIEW ORDER PAGE BANK ACCOUNT DETAILS
function view_order_custom_payment_instruction( $order ){
    // Only for "on-hold" and "processing" order statuses and on 'view-order' page
    if( in_array( $order->get_status(), array( 'on-hold', 'processing' ) ) && is_wc_endpoint_url( 'view-order' ) && $order->get_payment_method() == 'bacs' ){
        // The "Payment instructions" will be displayed with that:
        do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() );
    }
}
add_action( 'woocommerce_order_details_after_order_table', 'view_order_custom_payment_instruction', 5, 1); // Email notifications

/**
 * Exclude products from a particular category on the shop page
 */
function custom_pre_get_posts_query( $q ) {
	if (is_shop()) {
		$tax_query = (array) $q->get( 'tax_query' );
	    $tax_query[] = array(
	           'taxonomy' => 'product_cat',
	           'field' => 'slug',
	           'terms' => array( 'zapowiedz' ), // Don't display products in the clothing category on the shop page.
	           'operator' => 'NOT IN'
	    );
	    $q->set( 'tax_query', $tax_query );
	}
}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );


// https://www.tychesoftwares.com/how-to-add-charges-or-discounts-for-different-payment-methods-in-woocommerce/
// "Za pobraniem" payment
function ts_add_discount( $cart_object ) {
    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
    // Mention the payment method e.g. cod, bacs, cheque or paypal
    $payment_method = 'cod';
    // The percentage to apply
    $percent = 0.5; // 0.5%
    $shipping_total = $cart_object->get_shipping_total();
	// var_dump($cart_object->get_shipping_total());

    $chosen_payment_method = WC()->session->get('chosen_payment_method');  //Get the selected payment method
    if( $payment_method == $chosen_payment_method ){

       $label_text = __( "Za pobraniem" );

       // Calculating percentage
       $discount = number_format($shipping_total * $percent, 2);

       // Adding the discount
       $cart_object->add_fee( $label_text, $discount, false );
    }
}
add_action( 'woocommerce_cart_calculate_fees','ts_add_discount', 20, 1 );


// change checkout shipping method labels name
function filter_woocommerce_cart_shipping_method_full_label( $label, $method ) {
    // NOT returns true on the checkout page.
    if ( ! is_checkout() )
        return $label;
    $label = str_replace( "Odbiór osobisty: ", "Odbiór osobisty", $label );
	$label = str_replace( "Poczta Polska: ", "Poczta Polska", $label );
	$label = str_replace( "InPost Paczkomat: ", "InPost Paczkomat", $label );
	$label = str_replace( "InPost Kurier: ", "InPost Kurier", $label );
    return $label;
}
add_filter( 'woocommerce_cart_shipping_method_full_label', 'filter_woocommerce_cart_shipping_method_full_label', 10, 2 );

// placeholder for checkout "create account"
function custom_override_checkout_fields( $fields ) {
    $fields['account']['account_username']['placeholder'] = __( 'adres e-mail', 'woocommerce' );
    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields', 90, 1 );

// shipping cost based on order total - free shipping rate
add_filter( 'woocommerce_package_rates', 'custom_shipping_costs', 20, 2 );
function custom_shipping_costs( $rates, $package ) {
    // New shipping cost (can be calculated)
	$price_amount = 250;
    $zero_cost = 0;
	$cart_total = WC()->cart->get_cart_contents_total();

	if( $cart_total > $price_amount ) {
		foreach( $rates as $rate_key => $rate ){
	        // Excluding free shipping methods
	        if( $rate->method_id != 'free_shipping'){
	            // Set rate cost
	            $rates[$rate_key]->cost = $zero_cost;
	        }
	    }
	}
	return $rates;
}
