<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<ul class="accordion billing-accordion" data-accordion data-allow-all-closed="true" data-multi-expand="true" data-slide-speed="500">
	<li class="accordion-item" data-accordion-item>
		<?php if ( is_user_logged_in() ): ?>
		<a href="#" class="accordion-title">Dane płatności
			<img class="accordion-arrow" src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-up.svg" alt="" role="button">
		</a>
		<?php else: ?>
		<a href="#" class="accordion-title">Zakupy bez zakładania konta
			<img class="accordion-arrow" src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-up.svg" alt="" role="button">
		</a>
		<?php endif; ?>
		<div class="accordion-content" data-tab-content>

<div class="woocommerce-billing-fields">

	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3><?php esc_html_e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3><?php esc_html_e( 'Twoje dane', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
		<?php
		$fields = $checkout->get_checkout_fields( 'billing' );

		foreach ( $fields as $key => $field ) {
			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
		}
		?>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>
