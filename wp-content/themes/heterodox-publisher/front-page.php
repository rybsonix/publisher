<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
*/

get_header(); ?>

<div class="content">

	<!-- <div class="shape-bottom"></div> -->

	<div class="inner-content grid-x">

		<main class="main small-12 large-12 cell" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php get_template_part( 'parts/home', 'main-slider' ); ?>

                <?php get_template_part( 'parts/home', 'products' ); ?>

				<?php get_template_part( 'parts/home', 'about' ); ?>

			<?php endwhile; endif; ?>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
