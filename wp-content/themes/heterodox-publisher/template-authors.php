<?php
/*
Template Name: Autorzy
*/

get_header(); ?>

<div class="grid-container">

	<div class="content archive-author-taxonomy">

		<div class="inner-content grid-x grid-margin-x">

			<main class="main small-12 cell" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page-authors' ); ?>

				<?php endwhile; endif; ?>

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->
	
</div>

<?php get_footer(); ?>
