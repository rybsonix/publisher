(function( root, $, undefined ) {

    $(function () {

        $( "form.checkout" ).on( "click", ".woocommerce-checkout-review-order .btn-plus, .woocommerce-checkout-review-order .btn-minus", function( e ) {
            e.preventDefault();
            const isNegative = $(e.target).closest('.woocommerce-checkout-review-order .btn-minus').is('.woocommerce-checkout-review-order .btn-minus');
            const input = $(e.target).closest('.product-quantity').find('input');
            if (input.is('input')) {
                input[0][isNegative ? 'stepDown' : 'stepUp']()
            }
            var data = {
                action: 'update_order_review',
                security: wc_checkout_params.update_order_review_nonce,
                post_data: $( 'form.checkout' ).serialize()
            };

            jQuery.post( add_quantity.ajax_url, data, function( response ){
                $( 'body' ).trigger( 'update_checkout' );
            });
        });

    });

} ( this, jQuery ));
