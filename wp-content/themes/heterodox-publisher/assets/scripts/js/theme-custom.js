(function( root, $, undefined ) {

    $(function () {

        // variables
        var heroEscapeButton = $('.hero .button-underline'),
        hero = $('.hero'),
        offcanvasBox = $('.off-canvas-wrapper');

        // cookie for animation
        function setCookie(name,value,days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days*24*60*60*1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "")  + expires + "; path=/";
        }
        function getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        }

        var heroFunction = function(){
            hero.css('transform', 'translateY(-100vh)');
            $('.hero h1').css('opacity', '.1');
            heroEscapeButton.css('opacity', '.1');
            offcanvasBox.addClass('active');
            setCookie('escape','true',7);
        }
        heroEscapeButton.click(heroFunction);
        setTimeout(heroFunction,7000);

        // hero animation
        setTimeout(function(){
            $('.hero h1').addClass('active');
        },1000)

        function light(){
            var heroTitle = $('.hero__title h1'),
            rect = heroTitle[0].getBoundingClientRect();
            $(document).mousemove(function(e){
                heroTitle.css({
                    'backgroundPosition':(e.pageX - rect.x - 150) + 'px' + ' ' + (e.pageY - rect.y - 150) + 'px',
                    'backgroundSize': '300px 300px'
                })
            });
        };

        setTimeout(function(){
            light();
        },1100);
        $(window).resize(function(){
            setTimeout(function(){
                light();
            },1100);
        });

        var hamMenuButton = $('.ham-menu-button');
        $('#off-canvas').on('opened.zf.offCanvas', function() {
            hamMenuButton.addClass('open');
        });
        $('.off-canvas-wrapper').on('close.zf.offCanvas', function() {
            hamMenuButton.removeClass('open');
        });

        // Swiper
        var homeText = new Swiper('.home-slider__text .swiper-container', {
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
            slidesPerView: 'auto',
            loopedSlides: 3,
            loop: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            speed: 700,
        });
        var homeImg = new Swiper('.home-slider__img .swiper-container', {
            loop:true,
            thumbs: {
                swiper: homeText,
            },
            // autoplay: {
            //     delay: 6000,
            //     disableOnInteraction: false
            // },
            speed: 500,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            loopedSlides: 3,
            coverflowEffect: {
                rotate: 0,
                stretch: 300,
                depth: 300,
                modifier: 1,
                slideShadows: false,
            },
        });

        $(document).on('click', '.cart .btn-plus, .cart .btn-minus', function(e) {
            e.preventDefault();

            $('.woocommerce .cart .actions button.button').removeAttr( 'disabled' ).attr('aria-disabled','false');
            const isNegative = $(e.target).closest('.cart .btn-minus').is('.cart .btn-minus');
            const input = $(e.target).closest('.product-quantity').find('input');
            if (input.is('input')) {
                input[0][isNegative ? 'stepDown' : 'stepUp']()
            }

            if(input.data('old-value') === undefined){
                input.data('old-value', 0);
            }
            if(input.val() == undefined || input.val() == "") return;
            var newVal = parseInt(input.val());
            var oldVal = parseInt(input.data('old-value'));
            if (oldVal != newVal){
                $('[name="update_cart"]').trigger('click');
            }
            input.data('old-value', input.val());
        })

        var coupon = $(".checkout_coupon");
        coupon.insertAfter('.shop_table.woocommerce-checkout-review-order-table');
        coupon.css('opacity','1');

        var columnTwo = $('.woocommerce .col2-set .col-2');
        var columnTwoAbs = $('.woocommerce .col2-set .col-2 .col-2-abs-container').height();
        $(window).resize(function(){
            columnTwoAbs = $('.woocommerce .col2-set .col-2 .col-2-abs-container').height();
        });

        function setHeight(){
            columnTwo.css({
                'padding-bottom': columnTwoAbs + 'px'
            });
        };
        setHeight();
        $(window).resize(function(){
            setHeight();
        });

        $('.billing-accordion .accordion-title').click(function(){
            $(this).parents('.billing-accordion').next('.woocommerce-account-fields').toggleClass('active');
        })

        $('.woocommerce-account-fields .accordion-title').click(function(){
            $(this).next('.accordion-content').find('#createaccount').click();
        });

        $(".checkout_coupon button").click(function(){
            columnTwo.css({
                'padding-bottom': columnTwoAbs + 'px',
                'margin-bottom': '100px',
                'transition-delay':'0.35s'
            });
        });

        $('.login-form-container .form-row-first input').attr('placeholder','użytkownik lub adres e-mail');
        $('.login-form-container .form-row-last input').attr('placeholder','hasło');
        $('.login-form-container button.button').addClass('button-underline').append('<img src="/wp-content/themes/heterodox-publisher/assets/images/right-arrow.svg" alt="" role="button">');
        $('.woocommerce-billing-fields #billing_first_name, .woocommerce-shipping-fields #shipping_first_name').attr('placeholder','imię');
        $('.woocommerce-billing-fields #billing_last_name, .woocommerce-shipping-fields #shipping_last_name').attr('placeholder','nazwisko');
        $('.woocommerce-billing-fields #billing_company, .woocommerce-shipping-fields #shipping_company').attr('placeholder','nazwa firmy (opcjonalnie)');
        $('.woocommerce-billing-fields #billing_postcode, .woocommerce-shipping-fields #shipping_postcode').attr('placeholder','kod pocztowy');
        $('.woocommerce-billing-fields #billing_city, .woocommerce-shipping-fields #shipping_city').attr('placeholder','miasto');
        $('.woocommerce-billing-fields #billing_phone').attr('placeholder','telefon');
        $('.woocommerce-billing-fields #billing_email').attr('placeholder','adres e-mail');

        $('.woocommerce-checkout-payment-gateways .input-radio').wrap('<div class="form-group"></div>');
        $('.woocommerce-checkout-payment-gateways .form-group').append('<span class="checkmark"></span>');

        $('.woocommerce-checkout .place-order').append('<img class="place-order-arrow" src="/wp-content/themes/heterodox-publisher/assets/images/right-arrow-big.svg" alt="" role="button">');

        var bankNumber = $('.woocommerce ul.order_details li.account_number strong').text();
        var bankSplit = bankNumber.split('');
        var finalAccount = '';
        for (var i = 0; i < bankSplit.length; i++){
            if(i == 1 || i == 5 || i == 9 || i == 13 || i == 17 || i == 21){
                finalAccount += bankSplit[i] + ' ';
            } else{
                finalAccount += bankSplit[i];
            }
        }
        $('.woocommerce ul.order_details li.account_number strong').text(finalAccount);

        $('.woocommerce-page.archive .widget .widgettitle').click(function(e){
            e.preventDefault();
            $(this).toggleClass('active');
            $('.woocommerce-page.archive .widget ul').slideToggle();
        });

        $('#payment input[type="radio"]').change(function(){
			$('body').trigger('update_checkout');
		})

        $('#shipping_method input[type="radio"]').change(function(){
            $('body').on( 'update_checkout ', function(){
                setTimeout(function(){
                    location.reload();
                },1000)
            });
		})

    });

} ( this, jQuery ));
