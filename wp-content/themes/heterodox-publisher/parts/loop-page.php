<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<?php
	$login = '/moje-konto/';
	$registration = '/moje-konto/?action=register';
	$currentpage = $_SERVER['REQUEST_URI'];
	$page_title = '';
	if($currentpage == $login){
		$page_title = is_user_logged_in() ? 'MOJE KONTO' : 'LOGOWANIE';
	} elseif($currentpage == $registration){
		$page_title = 'REJESTRACJA';
	} else {
		$page_title = get_the_title();
	}
	?>

	<header class="article-header">
		<h1 class="page-title"><?php echo $page_title; ?></h1>
	</header> <!-- end article header -->

	<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	}
	?>

    <section class="entry-content" itemprop="text">
	    <?php the_content(); ?>
	</section> <!-- end article section -->

	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</article> <!-- end article -->
