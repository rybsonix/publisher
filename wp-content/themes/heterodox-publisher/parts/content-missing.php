<?php
/**
 * The template part for displaying a message that posts cannot be found
 */
?>

<div class="post-not-found">

	<?php if ( is_search() ) : ?>

		<header class="article-header">
			<h1><?php _e( 'Niestety nic nie znaleziono.', 'jointswp' );?></h1>
		</header>

		<section class="entry-content">
			<p><?php _e( 'Spróbuj jeszcze raz.', 'jointswp' );?></p>
		</section>

		<section class="search">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				<label>
					<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'jointswp' ) ?></span>
					<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'szukaj...', 'jointswp' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointswp' ) ?>" />
				</label>
				<div class="form-group">
					<input type="submit" class="search-submit button button-underline" value="<?php echo esc_attr_x( 'wyszukaj', 'jointswp' ) ?>" />
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
				</div>
			</form>
		</section> <!-- end search section -->

		<!-- <footer class="article-footer">
			<p><?php //_e( 'This is the error message in the parts/content-missing.php template.', 'jointswp' ); ?></p>
		</footer> -->

	<?php else: ?>

		<header class="article-header">
			<h1><?php _e( 'Oops, Post Not Found!', 'jointswp' ); ?></h1>
		</header>

		<section class="entry-content">
			<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'jointswp' ); ?></p>
		</section>

		<section class="search">
		    <p><?php get_search_form(); ?></p>
		</section> <!-- end search section -->

		<footer class="article-footer">
		  <p><?php _e( 'This is the error message in the parts/content-missing.php template.', 'jointswp' ); ?></p>
		</footer>

	<?php endif; ?>

</div>
