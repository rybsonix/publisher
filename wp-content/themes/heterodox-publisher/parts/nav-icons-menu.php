<div class="top-bar-right">
	<div class="header__icons">
		<ul>
			<li>
				<a href="#" data-open="searchModal">
					<svg xmlns="http://www.w3.org/2000/svg" width="22.662" height="22.917" viewBox="0 0 22.662 22.917">
						<g id="Group_4" data-name="Group 4" transform="translate(-98 -80.294)">
							<circle id="Ellipse_5" data-name="Ellipse 5" cx="7.895" cy="7.895" r="7.895" transform="translate(99 81.294)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
							<line id="Line_2" data-name="Line 2" x1="4.797" y1="4.797" transform="translate(114.451 97)" fill="none" stroke="#161615" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
						</g>
					</svg>
				</a>
				<div class="large reveal" id="searchModal" data-reveal data-close-on-click="false">
					<div class="header__search">
						<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
							<label>
								<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'jointswp' ) ?></span>
								<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Poczytaj...', 'jointswp' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointswp' ) ?>" />
							</label>
							<input type="submit" class="search-submit button" value="" />
							<svg xmlns="http://www.w3.org/2000/svg" width="22.662" height="22.917" viewBox="0 0 22.662 22.917">
								<g id="Group_4" data-name="Group 4" transform="translate(-98 -80.294)">
									<circle id="Ellipse_5" data-name="Ellipse 5" cx="7.895" cy="7.895" r="7.895" transform="translate(99 81.294)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
									<line id="Line_2" data-name="Line 2" x1="4.797" y1="4.797" transform="translate(114.451 97)" fill="none" stroke="#161615" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
								</g>
							</svg>
						</form>
					</div>
					<button class="close-button" data-close aria-label="Close modal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</li>
			<li class="account-item">
				<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="21.38" height="21.489" viewBox="0 0 21.38 21.489">
						<g id="Group_5" data-name="Group 5" transform="translate(-140.772 -80.294)">
							<circle id="Ellipse_6" data-name="Ellipse 6" cx="5.229" cy="5.229" r="5.229" transform="translate(146.233 81.294)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
							<path id="Path_40" data-name="Path 40" d="M156.692,94.773a6.4,6.4,0,0,1,4.46,5.855h0c0,3.474-19.38,3.474-19.38,0h0a6.379,6.379,0,0,1,4.36-5.818" transform="translate(0 -2.45)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
						</g>
					</svg>
				</a>
				<?php if(is_user_logged_in()):?>
				<a class="menu-logout-url" href="<?php echo esc_url( wc_get_account_endpoint_url( 'customer-logout' ) ); ?>">Wyloguj</a>
				<?php endif; ?>
			</li>
			<li class="basket-item">
				<a href="<?php echo wc_get_cart_url();?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="20.773" height="23.862" viewBox="0 0 20.773 23.862">
						<g id="Group_6" data-name="Group 6" transform="translate(-182.993 -80.843)">
							<path id="Path_41" data-name="Path 41" d="M197.076,85.512H187.09a3.107,3.107,0,0,0-3.1,3.1v5.781a3.107,3.107,0,0,0,3.1,3.1h9.986a3.107,3.107,0,0,0,3.1-3.1V81.843h2.592" transform="translate(0)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
							<circle id="Ellipse_7" data-name="Ellipse 7" cx="1.827" cy="1.827" r="1.827" transform="translate(183.993 100.05)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
							<circle id="Ellipse_8" data-name="Ellipse 8" cx="1.827" cy="1.827" r="1.827" transform="translate(196.543 100.05)" fill="none" stroke="#161615" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
						</g>
					</svg>
					<?php global $woocommerce;
						$counter = $woocommerce->cart->cart_contents_count;
						if($counter == 0) {
							echo '';
						} else {
							echo '<span class="pub-cart-count">'.$counter.'</span>';
						};
					?>
				</a>
			</li>
			<li class="show-for-xlarge">
				<a href="https://www.instagram.com/heterodoxwydawnictwo/">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="19.923" height="19.927" viewBox="0 0 48 48" version="1.1"><g id="Icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Black" transform="translate(-500.000000, -160.000000)" fill="#161615"><path d="M524 160c-6.518 0-7.335.028-9.895.144-2.555.117-4.3.523-5.826 1.116-1.578.613-2.917 1.434-4.25 2.768-1.335 1.334-2.156 2.673-2.769 4.251-.593 1.527-1 3.271-1.116 5.826-.116 2.56-.144 3.377-.144 9.895s.028 7.335.144 9.895c.117 2.555.523 4.3 1.116 5.826.613 1.578 1.434 2.917 2.768 4.25 1.334 1.335 2.673 2.156 4.251 2.77 1.527.592 3.271.998 5.826 1.115 2.56.116 3.377.144 9.895.144s7.335-.028 9.895-.144c2.555-.117 4.3-.523 5.826-1.116 1.578-.613 2.917-1.434 4.25-2.768 1.335-1.334 2.156-2.673 2.77-4.251.592-1.527.998-3.271 1.115-5.826.116-2.56.144-3.377.144-9.895s-.028-7.335-.144-9.895c-.117-2.555-.523-4.3-1.116-5.826-.613-1.578-1.434-2.917-2.768-4.25-1.334-1.335-2.673-2.156-4.251-2.769-1.527-.593-3.271-1-5.826-1.116-2.56-.116-3.377-.144-9.895-.144zm0 4.324c6.408 0 7.167.025 9.698.14 2.34.107 3.61.498 4.457.827 1.12.435 1.92.955 2.76 1.795.839.84 1.359 1.64 1.794 2.76.33.845.72 2.116.827 4.456.115 2.53.14 3.29.14 9.698s-.025 7.167-.14 9.698c-.107 2.34-.498 3.61-.827 4.457-.435 1.12-.955 1.92-1.795 2.76-.84.839-1.64 1.359-2.76 1.794-.845.33-2.116.72-4.456.827-2.53.115-3.29.14-9.698.14-6.409 0-7.168-.025-9.698-.14-2.34-.107-3.61-.498-4.457-.827-1.12-.435-1.92-.955-2.76-1.795-.839-.84-1.359-1.64-1.794-2.76-.33-.845-.72-2.116-.827-4.456-.115-2.53-.14-3.29-.14-9.698s.025-7.167.14-9.698c.107-2.34.498-3.61.827-4.457.435-1.12.955-1.92 1.795-2.76.84-.839 1.64-1.359 2.76-1.794.845-.33 2.116-.72 4.456-.827 2.53-.115 3.29-.14 9.698-.14zm0 7.352c-6.807 0-12.324 5.517-12.324 12.324 0 6.807 5.517 12.324 12.324 12.324 6.807 0 12.324-5.517 12.324-12.324 0-6.807-5.517-12.324-12.324-12.324zM524 192a8 8 0 1 1 0-16 8 8 0 0 1 0 16zm15.691-20.811a2.88 2.88 0 1 1-5.76 0 2.88 2.88 0 0 1 5.76 0z" id="Instagram"/></g></g></svg>

				</a>
			</li>
			<li class="show-for-xlarge">
				<a href="https://www.facebook.com/WydawnictwoHeterodox/">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Layer_1" version="1.1" viewBox="0 0 100 100" width="20" height="30" xml:space="preserve"><defs><path id="SVGID_1_"/></defs><path stroke="#161615" d="M38.078 22.431v12.391H29v15.152h9.078V95h18.648V49.975H69.24s1.172-7.265 1.74-15.209H56.797v-10.36c0-1.548 2.033-3.631 4.043-3.631H71V5.001H57.186C37.617 5 38.078 20.167 38.078 22.431"/>></svg>
				</a>
			</li>
		</ul>
	</div>
</div>
