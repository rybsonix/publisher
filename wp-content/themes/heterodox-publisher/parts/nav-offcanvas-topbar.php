<?php
/**
* The off-canvas menu uses the Off-Canvas Component
*
* For more info: http://jointswp.com/docs/off-canvas-menu/
*/
?>
<?php $image = get_field('main_logo_black','option'); ?>
<?php $image_mobile = get_field('logo_mobile','option'); ?>
<div class="top-bar" id="top-bar-menu">
	<div class="top-bar-left">
		<ul class="menu">
			<li>
				<a class="logo-desktop" href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" /></a>
				<a class="logo-mobile"href="<?php echo home_url(); ?>"><img src="<?php echo esc_url($image_mobile['url']); ?>" alt="<?php echo esc_attr($image_mobile['alt']); ?>" /></a>
			</li>
		</ul>
	</div>
	<div class="top-bar-center show-for-xlarge">
		<?php joints_top_nav(); ?>
	</div>

	<?php get_template_part( 'parts/nav', 'icons-menu' ); ?>

	<div class="top-bar-right-mobile hide-for-xlarge">
		<ul class="menu">
			<li>
				<div class="ham-menu-button" data-toggle="off-canvas">
					<div class="icon"></div>
				</div>
			</li>
			<!-- <li><a data-toggle="off-canvas"><?php //_e( 'Menu', 'jointswp' ); ?></a></li> -->
		</ul>
	</div>
</div>
