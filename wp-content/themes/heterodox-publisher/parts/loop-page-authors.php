<?php
/**
* Template part for displaying page content in page.php
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->

	<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	}
	?>

	<section class="entry-content authors" itemprop="text">
		<div class="grid-x grid-margin-x">
			<?php
			$terms = get_terms( 'pa_autor' );

			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):
				foreach ( $terms as $term ):
					$image = get_field('author_tax_img', $term->taxonomy . '_' . $term->term_id);
					$author_excerpt = get_field('author_tax_excerpt', $term->taxonomy . '_' . $term->term_id);
					?>
					<div class="cell small-12 medium-6 large-4">
						<div class="authors__item">
							<a href="/autor/<?php echo $term->slug; ?>" class="authors__item-img">
								<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								<p><?php echo $author_excerpt; ?></p>
							</a>
							<h2 class="authors__item-name"><?php echo $term->name; ?></h2>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</section> <!-- end article section -->

</article> <!-- end article -->
