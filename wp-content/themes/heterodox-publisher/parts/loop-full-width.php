<?php
/**
 * Template part for displaying page content in page.php
 */
?>
<?php
$image = get_field('page_left_img');
$footer = get_field('page_right_text_full');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->

	<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
	}
	?>

    <section class="entry-content" itemprop="text">
		<?php if( $image ):?>
		<div class="page__main-info">
			<div class="grid-x">
				<div class="cell small-12 large-6">
					<div class="page__main-info-img">
						<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
					</div>
				</div>
				<div class="cell small-12 large-6">
					<div class="page__main-info-text">
						<header>
							<h3><?php the_field('page_right_text_head');?></h3>
						</header>
						<div class="page__main-info-cols">
							<p><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pin-delete.svg" alt=""><?php the_field('page_right_text_left_column');?></p>
							<p><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pin-user.svg" alt=""><?php the_field('page_right_text_right_column');?></p>
						</div>
						<?php if($footer):?>
						<footer>
							<p><?php echo $footer;?></p>
						</footer>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div>
		<?php endif;?>
		<div class="page__content-container">
			<?php the_content(); ?>
		</div>
	</section> <!-- end article section -->

</article> <!-- end article -->
