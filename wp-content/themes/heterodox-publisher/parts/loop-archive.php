<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>

<div class="cell small-12 medium-6 large-4">

	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

		<header class="content-header">
			<a class="article-header__img" href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		</header> <!-- end article header -->

		<section class="entry-content" itemprop="text">
			<?php fl_excerpt($post->ID, 'fl_archive'); ?>
			<a href="<?php the_permalink(); ?>" class="button-underline">zobacz więcej
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
			</a>
		</section> <!-- end article section -->

		<!-- <footer class="article-footer">
	    	<p class="tags"><?php //the_tags('<span class="tags-title">' . __('Tags:', 'jointswp') . '</span> ', ', ', ''); ?></p>
		</footer>  -->
		<!-- end article footer -->

	</article> <!-- end article -->

</div>
