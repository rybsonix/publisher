<?php $image = get_field('main_logo_white','option'); ?>
<div class="hero">
    <div class="hero__container">
        <div class="hero__logo">
            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
        </div>
        <div class="hero__title">
            <h1><?php the_field('publisher_main_text','option'); ?></h1>
        </div>
        <div class="hero__escape">
            <a href="#" class="button-underline">zapraszamy
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
            </a>
        </div>
    </div>
</div>
