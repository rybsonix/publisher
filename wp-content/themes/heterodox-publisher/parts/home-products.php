<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => 1,
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'tax_query'             => array(
        array(
            'taxonomy'      => 'product_cat',
            'terms'         => 17
        )
    )
);

$param = array(
    'post_type' => 'product',
    'posts_per_page' => 1,
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'tax_query'             => array(
        array(
            'taxonomy'      => 'product_cat',
            'terms'         => 19
        )
    )
);

$new_query = new WP_Query( $args );
$best_query = new WP_Query( $param ); ?>

    <section class="home-products section-main">
        <div class="grid-container">
            <div class="grid-x grid-margin-x">
                <?php if ( $new_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $new_query->have_posts() ) : $new_query->the_post(); ?>
                    <?php
                    global $product;
                    $title = $product->get_name();
                    $desc = $product->get_short_description();
                    $author = $product->get_attribute( 'pa_autor' );?>
                    <div class="cell small-12 large-6">
                        <header class="post-header">
                            <h2>Nowości</h2>
                        </header>
                        <main class="product-post-main">
                            <div class="product-post-main__img">
                                <?php
                                $thumbnail_id = get_post_thumbnail_id( $post->ID );
                                $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); ?>
                                <a class="product-post-main__img-link" href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail( 'full', array( 'alt' => $alt, 'class' => 'product-post-main__img-item' ) );?>
                                </a>
                                <div class="product-post-main__icons">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/i.svg" alt="" role="button">
                                    </a>
                                    <a href="/?add-to-cart=<?php echo $product->get_id();?>">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/basket.svg" alt="" role="button">
                                    </a>
                                </div>
                            </div>
                            <div class="product-post-main__text">
                                <div class="product-post-main__title">
                                    <h3>
                                        <a href="<?php the_permalink(); ?>"><?php echo $title; ?></a>
                                    </h3>
                                </div>
                                <div class="product-post-main__author">
                                    <h4><?php echo $author; ?></h4>
                                </div>
                                <div class="product-post-main__desc">
                                    <p><?php echo $desc; ?></p>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="button-underline">zobacz więcej
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
                                </a>
                            </div>
                        </main>
                    </div>
                <?php endwhile; ?>
                <!-- end of the loop -->
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>

            <?php if ( $best_query->have_posts() ) : ?>
            <!-- the loop -->
            <?php while ( $best_query->have_posts() ) : $best_query->the_post(); ?>
                <?php
                global $product;
                $title = $product->get_name();
                $desc = $product->get_short_description();
                $author = $product->get_attribute( 'pa_autor' );?>
                <div class="cell small-12 large-6">
                    <header class="post-header">
                        <h2>Bestseller</h2>
                    </header>
                    <main class="product-post-main">
                        <div class="product-post-main__img">
                            <?php
                            $thumbnail_id = get_post_thumbnail_id( $post->ID );
                            $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); ?>
                            <a class="product-post-main__img-link" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'full', array( 'alt' => $alt, 'class' => 'product-post-main__img-item' ) );?>
                            </a>
                            <div class="product-post-main__icons">
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/i.svg" alt="" role="button">
                                </a>
                                <a href="/?add-to-cart=<?php echo $product->get_id();?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/basket.svg" alt="" role="button">
                                </a>
                            </div>
                        </div>
                        <div class="product-post-main__text">
                            <div class="product-post-main__title">
                                <h3>
                                    <a href="<?php the_permalink(); ?>"><?php echo $title; ?></a>
                                </h3>
                            </div>
                            <div class="product-post-main__author">
                                <h4><?php echo $author; ?></h4>
                            </div>
                            <div class="product-post-main__desc">
                                <p><?php echo $desc; ?></p>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="button-underline">zobacz więcej
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
                            </a>
                        </div>
                    </main>
                </div>
            <?php endwhile; ?>
            <!-- end of the loop -->
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
            </div>
        </div>
    </section>
