<?php $image = get_field('home_down_img'); ?>
<section class="home-about section-main">
    <div class="grid-container">
        <div class="grid-x align-middle">
            <div class="cell small-12">
                <header class="post-header">
                    <h2><?php the_field('home_down_heading'); ?></h2>
                </header>
            </div>
            <div class="cell small-12 medium-4">
                <div class="home-about__img">
                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                </div>
            </div>
            <div class="cell small-12 medium-8">
                <div class="home-about__text">
                    <?php the_field('home_down_text'); ?>
                    <a href="<?php the_field('home_down_link'); ?>" class="button-underline">zobacz więcej
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
