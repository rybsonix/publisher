<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'ignore_sticky_posts' => 1,
    'tax_query' => array( array(
        'taxonomy' => 'product_cat',
        'field' => 'id',
        'terms' => array( 23 ), // HERE the product category to exclude
        'operator' => 'NOT IN',
    ) ),
);
$the_query = new WP_Query( $args ); ?>

<?php if ( $the_query->have_posts() ) : ?>

    <section class="home-slider section-main">
        <div class="grid-container">
            <div class="grid-x align-middle">
                <div class="cell small-12 large-5">
                    <div class="home-slider__img">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <div class="swiper-slide">
                                        <div class="home-slider__img-slide">
                                            <?php
                                            $thumbnail_id = get_post_thumbnail_id( $post->ID );
                                            $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
                                            the_post_thumbnail( 'full', array( 'alt' => $alt ) );
                                            ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
                <div class="cell small-12 large-7">
                    <div class="home-slider__text">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    <?php global $product; ?>
                                    <?php $title = $product->get_name(); ?>
                                    <div class="swiper-slide">
                                        <div class="home-slider__text-content">
                                            <h2><?php echo $title; ?></h2>
                                            <a href="<?php the_permalink(); ?>" class="button-underline">zobacz więcej
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
                                            </a>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
