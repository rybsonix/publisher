<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<header class="article-header">
		<div class="grid-container">
			<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
			<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
		</div>
    </header> <!-- end article header -->

    <section class="entry-content" itemprop="text">
		<div class="grid-container grid-container--small">
			<div class="entry-content__img">
				<?php the_post_thumbnail('full'); ?>
			</div>
			<?php the_content(); ?>
		</div>
	</section> <!-- end article section -->

	<!-- <footer class="article-footer">
		<?php //wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php //the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>
	</footer> -->
	 <!-- end article footer -->

	<?php //comments_template(); ?>

</article> <!-- end article -->
