<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>

	<div class="grid-container">

		<header class="article-header">
			<h1 class="page-title">Aktualności</h1>
		</header> <!-- end article header -->

		<?php
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
		}
		?>

		<div class="content blog-archive">

			    <main class="main" role="main">

					<div class="grid-x grid-margin-x">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<!-- To see additional archive styles, visit the /parts directory -->
							<?php get_template_part( 'parts/loop', 'archive' ); ?>

						<?php endwhile; ?>

						<div class="cell small-12">

							<?php joints_page_navi(); ?>

						</div>

						<?php endif; ?>

					</div>

			    </main> <!-- end #main -->

		</div> <!-- end #content -->

	</div>

<?php get_footer(); ?>
