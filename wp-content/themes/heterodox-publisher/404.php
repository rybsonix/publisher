<?php
/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>

	<div class="content">

		<div class="grid-container">

			<div class="inner-content grid-x grid-margin-x">

				<main class="main small-12 cell" role="main">

					<article class="content-not-found">

						<header class="article-header">
							<h1><?php _e( 'Niestety nic nie znaleziono.', 'jointswp' ); ?></h1>
						</header> <!-- end article header -->

						<section class="entry-content">
							<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'jointswp' ); ?></p>
						</section> <!-- end article section -->

						<section class="search">
							<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
								<label>
									<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'jointswp' ) ?></span>
									<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'szukaj...', 'jointswp' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'jointswp' ) ?>" />
								</label>
								<div class="form-group">
									<input type="submit" class="search-submit button button-underline" value="<?php echo esc_attr_x( 'wyszukaj', 'jointswp' ) ?>" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/right-arrow.svg" alt="" role="button">
								</div>
							</form>
						</section> <!-- end search section -->

						<footer>
							<p>Możesz także wrócić na <a href="/">stronę główną</a>.</p>
						</footer>

					</article> <!-- end article -->

				</main> <!-- end #main -->

			</div> <!-- end #inner-content -->

		</div>

	</div> <!-- end #content -->

<?php get_footer(); ?>
