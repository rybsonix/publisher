#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inpost Paczkomaty\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-04 20:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.4; wp-5.8.1\n"
"X-Domain: inpost-paczkomaty"

#: includes/settings-inpost-paczkomaty.php:47
msgid "A maximum order amount"
msgstr ""

#: includes/settings-inpost-paczkomaty.php:46
msgid "A minimum order amount"
msgstr ""

#. Author of the plugin
msgid "Damian Ziarnik"
msgstr ""

#. Author URI of the plugin
msgid "https://profiles.wordpress.org/rimosfafora/"
msgstr ""

#. Name of the plugin
#: inpost-paczkomaty.php:59
msgid "Inpost Paczkomaty"
msgstr ""

#: includes/settings-inpost-paczkomaty.php:60
msgid "Maximum order amount"
msgstr ""

#: includes/settings-inpost-paczkomaty.php:51
msgid "Minimum order amount"
msgstr ""

#: includes/settings-inpost-paczkomaty.php:14
msgid "Paczkomaty"
msgstr ""

#: inpost-paczkomaty.php:60
msgid "Paczkomaty inpost shipping method"
msgstr ""

#. Description of the plugin
msgid "Plugin do obsługi paczkomatów inpost w woocommerce."
msgstr ""

#: inpost-paczkomaty.php:612 inpost-paczkomaty.php:625
#: inpost-paczkomaty.php:635
msgid "Selected Paczkomat"
msgstr ""

#: includes/settings-inpost-paczkomaty.php:40
msgid "Show this shipping method when..."
msgstr ""
