=== Inpost Paczkomaty ===
Contributors: rimosfafora
Donate link:
Tags: inpost, paczkomaty
Requires at least: 5.3
Tested up to: 5.8.1
Requires PHP: 7.2
Stable tag: 1.0.16
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Umożliwia dodanie Paczkomaty Inpost jako forma dostawy produktów. Zawiera mapkę gdzie można wybrać paczkomat w którym chce się odebrać przesyłkę.

== Description ==

Wtyczka umożliwia dodanie Paczkomatów Inpost jako forma dostawy w Woocoommerce. Zawiera mapkę gdzie można wybrać paczkomat w którym chce się odebrać przesyłkę. Wskazany paczkomat jest dodawany do zamówienia w panelu. Wtyczka jest bardzo prosta i intuicyjna dla każdego użytkownika.


== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png

== Frequently Asked Questions ==

= Jak dodać paczkomaty =

Po aktywowaniu wtyczki należy przejść do woocommerce -> ustawienia -> wysyłka-> kraj oraz dodać nową formę dostawy typu "paczkomaty inpost".

= Jak dodać darmową przesyłkę =

Należy dwa razy dodać paczkomaty jako formę wysyłki. Pierwsza forma np. koszt 20zł i "Pokaż tę metodę wysyłki, gdy... maksymalna wartość 60zł" oraz druga forma koszt 0zł i "Pokaż tę metodę wysyłki, gdy... minimalna wartość 60zł". Kwoty 20zł i 60zł to są wartości przykładowe.

= Czy będzie rozwinięcie dla kurierów =

Tak, w przyszłości planuje rozbudować wtyczkę.

= U mnie nie działa / znalazłem błąd, co zrobić? =

Napisz do mnie wiadomość a ja postaram się najszybciej to poprawić jak tylko możliwe. Możesz napisać na email: damian.ziarnik@o2.pl lub na linkedIn.

== Changelog ==

= 1.0.16 =
* Rozwiązanie błędu który był generowany w podglądzie customowej wiadomości

= 1.0.15 =
* Dodanie możliwości pokazywanie przesyłki w zakresie "od-do"

= 1.0.14 =
* Rozwiązanie problemu aby powiadomienia braly informacje o przesyłce na podstawie id przesyłki a nie nazwie

= 1.0.13 =
* Rozwiązanie problemu z pojawiającą się informacją o  paczkomacie w mailu gdy wybrana inna forma wysyłki

= 1.0.12 =
* Hotfix zwiazany z wysyłką po wersji 1.0.11.

= 1.0.11 =
* Modyfikacja powiadomień (email i strona podziękowania za zamówienie). Pokazywanie paczkomatu w tabelce.

= 1.0.10 =
* Dodanie akcji aby mozna było wyciągnąć dane paczkomatu do customowego powiadomienia mailowego

= 1.0.9 =
* Modyfikacja langów
* Poprawa Domain dla langów
* Dodanie możliwości przetłumaczenia dla innych języków
* Naprawa skryptu w panelu przy wyborze minimalnej/maksymalnej kwoty

= 1.0.8 =
* Dodanie modyfikatorów wysyłki tak aby można było ustawić do wybranej kwoty oraz od wybranej kwoty.

= 1.0.7 =
* poprawa wczytywania skryptu JavaScript w podstronach koszyka.

= 1.0.6 =
* dodanie klas "btn" oraz "button" aby przycisk w koszyku wykorzystywał style szablonu.

= 1.0.5 =
* Dodanie pola z samym numerem paczkomatu do panelu (potrzebne do intergracji z baselinkerem).

= 1.0.4 =
* Nie pokazywanie "wybrany paczkomat" gdy wybrana została inna forma wysyłki.
* Dodanie informacji o wybranym paczkomacie w potwierdzeniu mailowym do nowego zamówienia.
* Zablokowanie możliwości zakupu kiedy nie wybrano paczkomatu.
* Wywoływanie skryptów Inpostu (JS i CSS) tylko na stronie Koszyka i Kasy.

= 1.0.3 =
* Usunięcie przypadkowych znaków.
* Modyfikacja przesyłania zmiennych pomiędzy etapami.
* Usunięcie problemu z nieprawidłowymi przypisaniami paczkomatów.

= 1.0.2 =
* Modyfikacja przetwarzania danych w obrębie pluginu.
* Zamiana odwołań do plików.
* Zmiana permalinków.


= 1.0.1 =
* Dodanie ustawień na modalu.
* Dodanie ustawienia kosztów wysyłki według klas wysyłkowych.


= 1.0.0 =
* Dodanie możliwości wyboru paczkomatów.
* Mapka do paczkomatów w koszyku oraz checkoucie.
* Zapisywanie do panelu.
* Zapamiętywanie ostatniego wybranego paczkomatu.